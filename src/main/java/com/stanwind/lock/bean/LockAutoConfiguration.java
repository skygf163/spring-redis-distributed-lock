package com.stanwind.lock.bean;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * LockAutoConfiguration
 *
 * @author : Stan
 * @version : 1.0
 * @date :  2019-07-09 18:38
 **/
@ComponentScan({"com.stanwind.lock"})
@EnableAspectJAutoProxy
public class LockAutoConfiguration {

}
